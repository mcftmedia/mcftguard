package net.diamondmine.mcftguard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import net.diamondmine.mcftguard.listeners.GuardListeners;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Core of McftGuard
 * 
 * @author Jon la Cour
 * @version 1.0.7
 */
public class McftGuard extends JavaPlugin {
    public static HashMap<String, Integer> history = new HashMap<String, Integer>();
    public static final Logger logger = Logger.getLogger("Minecraft");
    public static List<String> worlds = new ArrayList<String>();
    public static Permission p;

    @Override
    public final void onDisable() {
        getServer().getServicesManager().unregisterAll(this);
        log("Version " + getDescription().getVersion() + " is disabled!", "info");
    }

    @Override
    public final void onEnable() {
        // Permissions
        if (!setupPermissions()) {
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        // Settings
        config = getConfig();
        checkConfig();
        loadConfig();

        PluginManager pm = getServer().getPluginManager();
        GuardListeners guardListeners = new GuardListeners(this);
        pm.registerEvents(guardListeners, this);
    }

    /**
     * This notifies everyone with the mcftguard.notify that someone placed a
     * lava or water block.
     * 
     * @param notification
     *            The string to send to all people with the mcftguard.notify
     *            permission.
     * @since 1.0.0
     */
    public static final void notify(final String notification) {
        for (Player player : Bukkit.getServer().getOnlinePlayers()) {
            if (has(player, "mcftguard.notify")) {
                player.sendMessage(notification);
            }
        }
    }

    /**
     * This adds to a players liquid placement count. The main purpose of this
     * is to prevent spamming of notifications.
     * 
     * @param name
     *            The players name to add to
     * @return History count
     */
    public static int history(final String name) {
        if (history.containsKey(name)) {
            int count = history.get(name) + 1;
            history.remove(name);
            history.put(name, count);
            return count;
        } else {
            history.put(name, 1);
            return 1;
        }
    }

    /**
     * Checks to make sure a permissions plugin has been loaded.
     * 
     * @return True if one is present, otherwise false.
     * @since 1.0.0
     */
    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(
                net.milkbowl.vault.permission.Permission.class);
        if (permissionProvider != null) {
            p = permissionProvider.getProvider();
        }
        return (p != null);
    }

    private FileConfiguration config;

    /**
     * Sets default configuration up if there isn't a configuration file
     * present.
     * 
     * @since 1.0.0
     */
    private void checkConfig() {
        config.options().copyDefaults(true);
        config.addDefault("blocked-worlds", worlds);
        config.options().copyDefaults(true);
        saveConfig();
    }

    /**
     * Loads configuration.
     * 
     * @since 1.0.0
     */
    private void loadConfig() {
        worlds = config.getStringList("blocked-worlds");
    }

    /**
     * Sends a message to the logger.
     * 
     * @param s
     *            The message to send
     * @param type
     *            The level (info, warning, severe)
     * @since 1.0.0
     */
    public static void log(final String s, final String type) {
        String message = "[McftGuard] " + s;
        String t = type.toLowerCase();
        if (t != null) {
            boolean info = t.equals("info");
            boolean warning = t.equals("warning");
            boolean severe = t.equals("severe");
            if (info) {
                logger.info(message);
            } else if (warning) {
                logger.warning(message);
            } else if (severe) {
                logger.severe(message);
            } else {
                logger.info(message);
            }
        }
    }

    /**
     * Checks if a user has a permission.
     * 
     * @param player
     *            The player's Player object.
     * @param permission
     *            The permission node to check.
     * @return boolean True if the user has the permission, otherwise false.
     * @since 1.0.0
     */
    public static boolean has(final Player player, final String permission) {
        return p.has(player, permission);
    }

}
