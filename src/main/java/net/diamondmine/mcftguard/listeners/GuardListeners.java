package net.diamondmine.mcftguard.listeners;

import static net.diamondmine.mcftguard.McftGuard.has;
import static net.diamondmine.mcftguard.McftGuard.worlds;
import net.diamondmine.mcftguard.McftGuard;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;

import uk.co.oliwali.HawkEye.util.HawkEyeAPI;
import de.diddiz.LogBlock.Consumer;
import de.diddiz.LogBlock.LogBlock;

/**
 * Block listener.
 * 
 * @author Jon la Cour
 * @version 1.0.7
 */
public class GuardListeners implements Listener {
    private static boolean hawkeye = false;
    private static boolean logblock = false;
    private static Consumer lbconsumer = null;
    private final McftGuard plugin;
    Permission p;

    /**
     * This just allows the main class to use us.
     * 
     * @param instance
     *            The main class.
     */
    public GuardListeners(final McftGuard instance) {
        plugin = instance;
        p = McftGuard.p;

        Plugin hawkeyepl = Bukkit.getServer().getPluginManager().getPlugin("HawkEye");
        Plugin logblockpl = Bukkit.getServer().getPluginManager().getPlugin("LogBlock");
        if (hawkeyepl != null) {
            hawkeye = true;
        }
        if (logblockpl != null) {
            logblock = true;
            lbconsumer = ((LogBlock) logblockpl).getConsumer();
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onBlockFromTo(final BlockFromToEvent event) {
        Block block = event.getBlock();
        if (worlds.contains(block.getWorld().getName())) {
            if (event.isCancelled()) {
                return;
            }

            if ((block.getType() == Material.STATIONARY_WATER) || (block.getType() == Material.WATER) || (block.getType() == Material.STATIONARY_LAVA)
                    || (block.getType() == Material.LAVA)) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onBlockPlace(final BlockPlaceEvent event) {
        Block block = event.getBlock();
        int blockid = block.getTypeId();
        Player player = event.getPlayer();
        String name = player.getName();
        String world = player.getWorld().getName();
        String notification = ChatColor.DARK_RED + "[McftGuard] " + ChatColor.WHITE + name;
        if (worlds.contains(world)) {
            if (blockid == 10 && !has(player, "mcftguard.lava")) {
                block.setTypeId(11);
                if (hawkeye) {
                    HawkEyeAPI.addCustomEntry(plugin, "Placed Lava", player, block.getLocation(), block.getType().toString());
                }
                if (logblock) {
                    lbconsumer.queueBlockPlace(name, event.getBlockReplacedState());
                }
                int count = McftGuard.history(name);
                if ((count % 20) == 0) {
                    notification += " placed lava in " + world + ". " + ChatColor.GRAY + "(Liquid count: " + count + ")";
                    McftGuard.notify(notification);
                }
            }
            if (blockid == 8 && !has(player, "mcftguard.water")) {
                block.setTypeId(9);
                if (hawkeye) {
                    HawkEyeAPI.addCustomEntry(plugin, "Placed Water", player, block.getLocation(), block.getType().toString());
                }
                if (logblock) {
                    lbconsumer.queueBlockPlace(name, event.getBlockReplacedState());
                }
                int count = McftGuard.history(name);
                if ((count % 20) == 0) {
                    notification += " placed water in " + world + ". " + ChatColor.GRAY + "(Liquid count: " + count + ")";
                    McftGuard.notify(notification);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerBucketEmpty(final PlayerBucketEmptyEvent event) {
        Player player = event.getPlayer();
        Block block = event.getBlockClicked();
        String name = player.getName();
        String world = player.getWorld().getName();
        String notification = ChatColor.DARK_RED + "[McftGuard] " + ChatColor.WHITE + name;
        if (worlds.contains(world)) {
            if (event.getBucket() == Material.LAVA_BUCKET && !has(player, "mcftguard.lava")) {
                if (hawkeye) {
                    HawkEyeAPI.addCustomEntry(plugin, "Emptied lava bucket.", player, block.getLocation(), block.getType().toString());
                }
                if (logblock) {
                    lbconsumer.queueBlockPlace(name, event.getBlockClicked().getState());
                }
                int count = McftGuard.history(name);
                if ((count % 20) == 0) {
                    notification += " placed lava in " + world + ". " + ChatColor.GRAY + "(Liquid count: " + count + ")";
                    McftGuard.notify(notification);
                }
            }
            if (event.getBucket() == Material.WATER_BUCKET && !has(player, "mcftguard.water")) {
                if (hawkeye) {
                    HawkEyeAPI.addCustomEntry(plugin, "Emptied water bucket.", player, block.getLocation(), block.getType().toString());
                }
                if (logblock) {
                    lbconsumer.queueBlockPlace(name, event.getBlockClicked().getState());
                }
                int count = McftGuard.history(name);
                if ((count % 20) == 0) {
                    notification += " placed water in " + world + ". " + ChatColor.GRAY + "(Liquid count: " + count + ")";
                    McftGuard.notify(notification);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerQuit(final PlayerQuitEvent event) {
        McftGuard.history.remove(event.getPlayer().getName());
    }
}
