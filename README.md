# McftGuard

A Bukkit server plugin for Minecraft that prevents certain groups from placing lava and water.

This was made to suit the needs of the [Diamond Mine](http://diamondmine.net) server.

**[License](https://diamondmine.net/plugins/license)** -- **[Issues](https://mcftmedia.atlassian.net/issues/?jql=project=GUARD)** --  **[Builds](https://mcftmedia.atlassian.net/builds/browse/GUARD-GUARD)**
-- **[Download](http://diamondmine.net/plugins/download/McftGuard)** -- **[Bukkit Dev](http://dev.bukkit.org/server-mods/mcftguard/)**